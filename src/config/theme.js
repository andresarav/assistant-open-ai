
export const lightTheme = {
    background: '#f5f5f5',
    chatBackground: 'white',
    color: 'black',
    borderColor: '#ccc',
    userMessageBackground: 'none',
    userMessageColor: 'black',
    userMessageBorderColor: '#f0f0f0',
    botMessageBackground: '#f0f0f0',
    botMessageColor: 'black',
    errorMessageColor: 'red',
  };
  
  export const darkTheme = {
    background: '#121212',
    chatBackground: '#1e1e1e',
    color: 'white',
    borderColor: '#333',
    userMessageBackground: 'none',
    userMessageColor: 'white',
    userMessageBorderColor: '#333',
    botMessageBackground: '#333',
    botMessageColor: 'white',
    errorMessageColor: 'red',
  };