import OpenAI from "openai";

export const openai = new OpenAI({
    apiKey: process.env.REACT_APP_OPEN_AI_API_KEY, 
    dangerouslyAllowBrowser: true
});


export const getAssistant = async() => {
    const response = await openai.beta.assistants.retrieve("asst_XK00YOuCK6VqUeHq9MPtufF0");
    return response;
}

export const createThread = async () => {
    const thread = await openai.beta.threads.create();
    return thread;
  }