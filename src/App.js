import React, { useEffect, useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { FaSun, FaMoon } from 'react-icons/fa';
import { funcDebounces, sleep } from './utils'; // Import funcDebounces
import { getAssistant, createThread, openai } from './config/openAi'; // Import getAssistant and createThread
import { lightTheme, darkTheme } from "./config/theme"; // Import lightTheme and darkTheme



function Chat() {

  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [theme, setTheme] = useState(darkTheme);
  const [thread, setThread] = useState();
  const [ assistant, setAssistant ] = useState();

  const toggleTheme = () => setTheme(theme === lightTheme ? darkTheme : lightTheme);
  const init = async () => getAssistant().then((assistant) => setAssistant(assistant))

  const checkBotResponse = async () => {
    const res = await openai.beta.threads.messages.list(thread.id);
    if(res.data[0] && !res.data[0].content[0].text.value) return;
    const botResponse = res.data[0].content[0].text.value;
    setMessages(prevMessages => [...prevMessages, { text: botResponse, sender: 'bot' }]);
    setLoading(false);
  }

  const checkRunStatus = async (runId) => {
    const run = await openai.beta.threads.runs.retrieve(thread.id, runId);
    if (run.status === "completed") return checkBotResponse();
    await sleep(1000);
    return checkRunStatus(runId);
  }

  async function parlar(message) {
    if(!thread?.id) return 'Cargando...';
    await openai.beta.threads.messages.create(thread.id, { role: "user", content:message });
    const run = await openai.beta.threads.runs.create( thread.id, { assistant_id: assistant.id } );
    return checkRunStatus(run.id);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const message = event.target.elements.message.value;
    setMessages([...messages, { text: message, sender: 'user' }]);
    setLoading(true);
    parlar(message)
    event.target.elements.message.value = '';
  }

  useEffect(() => { 
    init();
  }, [])

  useEffect(() => {
    if (assistant) {
      funcDebounces({
        keyId: { assistantId: assistant.id },
        callback: () => createThread().then((thread) => setThread(thread)),
        waitRes: true,
        timeExect: 500,
        storageType: "localStorage"
      });
    }
  }, [assistant]);


  return (
    <Layout>
      <ThemeProvider theme={theme}>
        <ChatContainer>
          <ThemeSwitch theme={theme} toggleTheme={toggleTheme} />
            <ChatDisplay>
              {messages.map((message, index) => (
                message.sender === 'user' ? 
                  <UserMessage key={index}>{message.text}</UserMessage> :
                  <BotMessage key={index}>{message.text}</BotMessage>
              ))}
              {loading && <p>Cargando...</p>}
              {error && <ErrorMessage>{error}</ErrorMessage>}
            </ChatDisplay>
            <ChatInputForm onSubmit={handleSubmit} loading={loading} />
        </ChatContainer>
      </ThemeProvider>
    </Layout>
  );
}

export default Chat;




const ChatInputForm = ({ onSubmit, loading }) => (
  <ChatForm onSubmit={onSubmit}>
    <input type="text" name="message" required disabled={loading} style={{ padding: '10px', borderRadius: '5px', border: '1px solid #ccc' }} />
    <button type="submit" disabled={loading} style={{ marginLeft: '10px', backgroundColor: '#0084ff', color: 'white', border: 'none', borderRadius: '5px', padding: '10px' }}>
      {loading ? 'Cargando...' : 'Enviar'}
    </button>
  </ChatForm>
);


const Layout = styled.div`
  background-color: #121212; // Dark background
  min-height: 100vh; // Full height
`;

const ThemeSwitchCont = styled.div`
  display: flex;
  border: 1px solid #ccc;
  border-radius: 5px;
  overflow: hidden;
  position: absolute; // Added
  top: 10px; // Added
  right: 10px; // Added
`;

const ThemeSwitchButton = styled.button`
  flex: 1;
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 10px;
  color: ${props => props.active ? '#f1c40f' : '#ccc'};
  font-size: 20.4px; // Decreased by 15%
  cursor: ${props => props.active ? 'default' : 'pointer'};
`;

function ThemeSwitch({ theme, toggleTheme }) {
  return (
    <ThemeSwitchCont>
      <ThemeSwitchButton active={theme === lightTheme} onClick={() => theme !== lightTheme && toggleTheme(lightTheme)}>
        <FaSun />
      </ThemeSwitchButton>
      <ThemeSwitchButton active={theme === darkTheme} onClick={() => theme !== darkTheme && toggleTheme(darkTheme)}>
        <FaMoon />
      </ThemeSwitchButton>
    </ThemeSwitchCont>
  );
}




const ChatContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  max-width: 100%;
  height: calc(100vh - 20px);
  background-color: ${props => props.theme.background}; // Use theme color

  @media (min-width: 768px) {
    max-width: 600px;
    margin: 0 auto;
  }
`;

const ChatDisplay = styled.div`
  flex: 1;
  overflow-y: auto;
  padding: 10px;
  border: 1px solid ${props => props.theme.borderColor}; // Use theme color
  border-radius: 5px;
  margin-bottom: 10px;
  height: calc(100vh - 140px);
  background-color: ${props => props.theme.chatBackground}; // Use theme color
`;

const UserMessage = styled.p`
  border: 1px solid ${props => props.theme.userMessageBorderColor}; // Use theme color
  color: ${props => props.theme.userMessageColor}; // Use theme color
  padding: 10px;
  border-radius: 5px;
  margin-bottom: 10px;
  margin-top: 30px;
  align-self: flex-end;
  width: 100%;
  max-width: 600px;
  word-wrap: break-word;
  position: relative;
  box-sizing: border-box;

  &::before {
    content: 'Usuario';
    position: absolute;
    top: -20px;
    right: 0;
    font-size: 0.8em;
    color: ${props => props.theme.userMessageColor}; // Use theme color
  }
`;

const BotMessage = styled(UserMessage)`
  background-color: ${props => props.theme.botMessageBackground}; // Use theme color
  color: ${props => props.theme.botMessageColor}; // Use theme color

  &::before {
    content: 'JuancitoBot';
    right: auto;
    left: 0;
    color: ${props => props.theme.botMessageColor}; // Use theme color
  }
`;

const ErrorMessage = styled.p`
  color: ${props => props.theme.errorMessageColor}; // Use theme color
`;

const ChatForm = styled.form`
  display: grid;
  grid-template-columns: 1fr 150px;
  justify-content: space-between;
  background-color: white;
  padding: 10px;
  border-top: 1px solid #ccc;
`;

