
    /**
     * @param {Object} keyId - Objeto con la clave y el valor
     * @param {Function} callback - Funcion a ejecutar
     * @param {Boolean} waitRes - define si recibe como callback una promesa o una funcion
     * @param {Number} timeExect - Tiempo de espera para ejecutar la funcion
     * @param {String} storageType - Tipo de storage
     * @returns {Promise|Function}
     */

export const funcDebounces = ({
    keyId, 
    callback,  
    waitRes = false, 
    timeExect = 1000,
    storageType = "localStorage"
  }) => { 
  
    const storage = window[storageType]
    if(!Object.entries(keyId).length)return ;
  
    const [ dataKey, dataValue ] = Object.entries(keyId)[0]
    let storageData = storage.getItem(dataKey);
    
    if(storageData === dataValue)return ;
    storage.setItem(dataKey, dataValue);
  
    if(waitRes){
      return new Promise(async (resolve) => {
        setTimeout(async() => {
          storage.removeItem(dataKey);
        }, timeExect)
        const res = await callback()
        return resolve(res)
      });
    }
  
    setTimeout(() => {
      storage.removeItem(dataKey);
    }, timeExect)
    
    return callback()
  }


  /**
   * @param {Number} ms - Tiempo de espera
   * @returns {Promise}
   */
  export const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
